
*** <summary>
*** /blck/mfi_cl_PetApi definition
*** 
*** </summary>
class /blck/mfi_cl_PetApi definition create protected
    inheriting from /blck/api_cl.

  public section.
  
    constants mc_def_uri type string value 'https://petstore.swagger.io/v2'. "#EC NOTEXT
	
*** <summary> method PetApi->add_pet
*** Add a new pet to the store
*** </summary>
*** <param name="i_body">
*** Pet object that needs to be added to the store
*** </param>
*** <returns>  </returns>
    class-methods add_pet
      importing
        i_body type /blck/mfi_pet
      raising /blck/cx_api_exception.
*** <summary> method PetApi->delete_pet
*** Deletes a pet
*** </summary>
*** <param name="i_pet_id">
*** Pet id to delete
*** </param>
*** <param name="i_api_key">
*** 
*** </param>
*** <returns>  </returns>
    class-methods delete_pet
      importing
        i_pet_id type i
        i_api_key type string optional
      raising /blck/cx_api_exception.
*** <summary> method PetApi->find_pets_by_status
*** Finds Pets by status
*** </summary>
*** <param name="i_status">
*** Status values that need to be considered for filter
*** </param>
*** <returns> r_result (array) </returns>
    class-methods find_pets_by_status
      importing
        i_status type /blck/api_string_tt
      returning
        value(r_result) type /blck/mfi_pet_tt
      raising /blck/cx_api_exception.
*** <summary> method PetApi->find_pets_by_tags
*** Finds Pets by tags
*** </summary>
*** <param name="i_tags">
*** Tags to filter by
*** </param>
*** <returns> r_result (array) </returns>
    class-methods find_pets_by_tags
      importing
        i_tags type /blck/api_string_tt
      returning
        value(r_result) type /blck/mfi_pet_tt
      raising /blck/cx_api_exception.
*** <summary> method PetApi->get_pet_by_id
*** Find pet by ID
*** </summary>
*** <param name="i_pet_id">
*** ID of pet to return
*** </param>
*** <returns> r_result (pet) </returns>
    class-methods get_pet_by_id
      importing
        i_pet_id type i
      returning
        value(r_result) type /blck/mfi_pet
      raising /blck/cx_api_exception.
*** <summary> method PetApi->update_pet
*** Update an existing pet
*** </summary>
*** <param name="i_body">
*** Pet object that needs to be added to the store
*** </param>
*** <returns>  </returns>
    class-methods update_pet
      importing
        i_body type /blck/mfi_pet
      raising /blck/cx_api_exception.
*** <summary> method PetApi->update_pet_with_form
*** Updates a pet in the store with form data
*** </summary>
*** <param name="i_pet_id">
*** ID of pet that needs to be updated
*** </param>
*** <param name="i_name">
*** 
*** </param>
*** <param name="i_status">
*** 
*** </param>
*** <returns>  </returns>
    class-methods update_pet_with_form
      importing
        i_pet_id type i
        i_name type string optional
        i_status type string optional
      raising /blck/cx_api_exception.
*** <summary> method PetApi->upload_file
*** uploads an image
*** </summary>
*** <param name="i_pet_id">
*** ID of pet to update
*** </param>
*** <param name="i_additional_metadata">
*** 
*** </param>
*** <param name="i_file">
*** 
*** </param>
*** <returns> r_result (api_response) </returns>
    class-methods upload_file
      importing
        i_pet_id type i
        i_additional_metadata type string optional
        i_file type file optional
      returning
        value(r_result) type /blck/mfi_api_response
      raising /blck/cx_api_exception.

    class-methods config
      importing
        i_basepath type string default mc_def_uri
        i_auth     type ref to /blck/api_cl_auth optional.
        
  protected section.
  
    class-data mvs_basepath type string default mc_def_uri.
    class-data mcl_auth type ref to /blck/api_cl_auth.
        
endclass.

*** <summary>
*** /blck/mfi_cl_PetApi implementation
*** </summary>
class /blck/mfi_cl_PetApi implementation.

  method config.
    mvs_basepath = i_basepath.
    mcl_auth = i_auth.
  endmethod.

*** <summary> method PetApi->add_pet
*** Add a new pet to the store
*** </summary>
*** <param name="i_body">
*** Pet object that needs to be added to the store
*** </param>
*** <returns>  </returns>
  method add_pet.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'add_pet' "#EC NOTEXT
*        i_parameter = 'body' "#EC NOTEXT
*        i_value = i_body ).
        
    lvs_path = '/pet'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    

* handle body parameter
    data lr_data_in type /blck/mfi_pet.
    lr_data_in = i_body->get_structure( ).
    lvs_postbody = to_json( lr_data_in ).
    

    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_POST
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

* no return value
  endmethod.
  
*** <summary> method PetApi->delete_pet
*** Deletes a pet
*** </summary>
*** <param name="i_pet_id">
*** Pet id to delete
*** </param>
*** <param name="i_api_key">
*** 
*** </param>
*** <returns>  </returns>
  method delete_pet.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'delete_pet' "#EC NOTEXT
*        i_parameter = 'pet_id' "#EC NOTEXT
*        i_value = i_pet_id ).
        
    lvs_path = '/pet/{petId}'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{petId}'. "#EC NOTEXT
    <fs_string_map>-v = param_to_string( i_pet_id ).
    
    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    
    if i_api_key is not initial.
      append initial line to li_headerparams assigning <fs_string_map>.
      <fs_string_map>-k = 'api_key'. "#EC NOTEXT
      <fs_string_map>-v = param_to_string( i_api_key ).
    endif.


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_DELETE
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

* no return value
  endmethod.
  
*** <summary> method PetApi->find_pets_by_status
*** Finds Pets by status
*** </summary>
*** <param name="i_status">
*** Status values that need to be considered for filter
*** </param>
*** <returns> r_result (array) </returns>
  method find_pets_by_status.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'find_pets_by_status' "#EC NOTEXT
*        i_parameter = 'status' "#EC NOTEXT
*        i_value = i_status ).
        
    lvs_path = '/pet/findByStatus'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    
    if i_status is not initial.
      append initial line to li_queryparams assigning <fs_string_map>.
      <fs_string_map>-k = 'status'. "#EC NOTEXT
      <fs_string_map>-v = param_to_string( i_status ).
    endif.
    


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_GET
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

    data li_return type /blck/mfi_pet_tt.
    field-symbols:
      <strc_return> type /blck/mfi_pet,
      <inst_return> type ref to /blck/mfi_pet.
      
* load model into memory to ensure mapping is available..
    /blck/mfi_pet=>get( ).

    from_json(
      exporting
        i_json = lvs_respbody
      changing
        c_model = li_return ).
        
    loop at li_return assigning <strc_return>.
      append initial line to r_result assigning <inst_return>.
      create object <inst_return>.
      move-corresponding <strc_return> to <inst_return>->mr_pet.
      <inst_return>->assign_structure( changing c_structure = <inst_return>->mr_pet ).
    endloop.
        

  endmethod.
  
*** <summary> method PetApi->find_pets_by_tags
*** Finds Pets by tags
*** </summary>
*** <param name="i_tags">
*** Tags to filter by
*** </param>
*** <returns> r_result (array) </returns>
  method find_pets_by_tags.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'find_pets_by_tags' "#EC NOTEXT
*        i_parameter = 'tags' "#EC NOTEXT
*        i_value = i_tags ).
        
    lvs_path = '/pet/findByTags'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    
    if i_tags is not initial.
      append initial line to li_queryparams assigning <fs_string_map>.
      <fs_string_map>-k = 'tags'. "#EC NOTEXT
      <fs_string_map>-v = param_to_string( i_tags ).
    endif.
    


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_GET
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

    data li_return type /blck/mfi_pet_tt.
    field-symbols:
      <strc_return> type /blck/mfi_pet,
      <inst_return> type ref to /blck/mfi_pet.
      
* load model into memory to ensure mapping is available..
    /blck/mfi_pet=>get( ).

    from_json(
      exporting
        i_json = lvs_respbody
      changing
        c_model = li_return ).
        
    loop at li_return assigning <strc_return>.
      append initial line to r_result assigning <inst_return>.
      create object <inst_return>.
      move-corresponding <strc_return> to <inst_return>->mr_pet.
      <inst_return>->assign_structure( changing c_structure = <inst_return>->mr_pet ).
    endloop.
        

  endmethod.
  
*** <summary> method PetApi->get_pet_by_id
*** Find pet by ID
*** </summary>
*** <param name="i_pet_id">
*** ID of pet to return
*** </param>
*** <returns> r_result (pet) </returns>
  method get_pet_by_id.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'get_pet_by_id' "#EC NOTEXT
*        i_parameter = 'pet_id' "#EC NOTEXT
*        i_value = i_pet_id ).
        
    lvs_path = '/pet/{petId}'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{petId}'. "#EC NOTEXT
    <fs_string_map>-v = param_to_string( i_pet_id ).
    
    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_GET
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

    create object r_result.
    
    from_json(
      exporting
        i_json = lvs_respbody
      changing
        c_model = r_result->mr_pet ).
        
    r_result->assign_structure( changing c_structure = r_result->mr_pet ).
        

  endmethod.
  
*** <summary> method PetApi->update_pet
*** Update an existing pet
*** </summary>
*** <param name="i_body">
*** Pet object that needs to be added to the store
*** </param>
*** <returns>  </returns>
  method update_pet.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'update_pet' "#EC NOTEXT
*        i_parameter = 'body' "#EC NOTEXT
*        i_value = i_body ).
        
    lvs_path = '/pet'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    

* handle body parameter
    data lr_data_in type /blck/mfi_pet.
    lr_data_in = i_body->get_structure( ).
    lvs_postbody = to_json( lr_data_in ).
    

    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_PUT
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

* no return value
  endmethod.
  
*** <summary> method PetApi->update_pet_with_form
*** Updates a pet in the store with form data
*** </summary>
*** <param name="i_pet_id">
*** ID of pet that needs to be updated
*** </param>
*** <param name="i_name">
*** 
*** </param>
*** <param name="i_status">
*** 
*** </param>
*** <returns>  </returns>
  method update_pet_with_form.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'update_pet_with_form' "#EC NOTEXT
*        i_parameter = 'pet_id' "#EC NOTEXT
*        i_value = i_pet_id ).
        
    lvs_path = '/pet/{petId}'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{petId}'. "#EC NOTEXT
    <fs_string_map>-v = param_to_string( i_pet_id ).
    
    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    
* if (name != null) formParams.Add("name", ApiClient.ParameterToString(name)); // form parameter
* if (status != null) formParams.Add("status", ApiClient.ParameterToString(status)); // form parameter


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_POST
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

* no return value
  endmethod.
  
*** <summary> method PetApi->upload_file
*** uploads an image
*** </summary>
*** <param name="i_pet_id">
*** ID of pet to update
*** </param>
*** <param name="i_additional_metadata">
*** 
*** </param>
*** <param name="i_file">
*** 
*** </param>
*** <returns> r_result (api_response) </returns>
  method upload_file.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'upload_file' "#EC NOTEXT
*        i_parameter = 'pet_id' "#EC NOTEXT
*        i_value = i_pet_id ).
        
    lvs_path = '/pet/{petId}/uploadImage'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{petId}'. "#EC NOTEXT
    <fs_string_map>-v = param_to_string( i_pet_id ).
    
    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    
* if (additional_metadata != null) formParams.Add("additionalMetadata", ApiClient.ParameterToString(additional_metadata)); // form parameter
* if (file != null) fileParams.Add("file", ApiClient.ParameterToFile("file", file));


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_POST
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

    create object r_result.
    
    from_json(
      exporting
        i_json = lvs_respbody
      changing
        c_model = r_result->mr_api_response ).
        
    r_result->assign_structure( changing c_structure = r_result->mr_api_response ).
        

  endmethod.
  
endclass.

