
*** <summary>
*** /blck/mfi_cl_UserApi definition
*** 
*** </summary>
class /blck/mfi_cl_UserApi definition create protected
    inheriting from /blck/api_cl.

  public section.
  
    constants mc_def_uri type string value 'https://petstore.swagger.io/v2'. "#EC NOTEXT
	
*** <summary> method UserApi->create_user
*** Create user
*** </summary>
*** <param name="i_body">
*** Created user object
*** </param>
*** <returns>  </returns>
    class-methods create_user
      importing
        i_body type /blck/mfi_user
      raising /blck/cx_api_exception.
*** <summary> method UserApi->create_users_with_array_input
*** Creates list of users with given input array
*** </summary>
*** <param name="i_body">
*** List of user object
*** </param>
*** <returns>  </returns>
    class-methods create_users_with_array_input
      importing
        i_body type /blck/mfi_user_tt
      raising /blck/cx_api_exception.
*** <summary> method UserApi->create_users_with_list_input
*** Creates list of users with given input array
*** </summary>
*** <param name="i_body">
*** List of user object
*** </param>
*** <returns>  </returns>
    class-methods create_users_with_list_input
      importing
        i_body type /blck/mfi_user_tt
      raising /blck/cx_api_exception.
*** <summary> method UserApi->delete_user
*** Delete user
*** </summary>
*** <param name="i_username">
*** The name that needs to be deleted
*** </param>
*** <returns>  </returns>
    class-methods delete_user
      importing
        i_username type string
      raising /blck/cx_api_exception.
*** <summary> method UserApi->get_user_by_name
*** Get user by user name
*** </summary>
*** <param name="i_username">
*** The name that needs to be fetched. Use user1 for testing. 
*** </param>
*** <returns> r_result (user) </returns>
    class-methods get_user_by_name
      importing
        i_username type string
      returning
        value(r_result) type /blck/mfi_user
      raising /blck/cx_api_exception.
*** <summary> method UserApi->login_user
*** Logs user into the system
*** </summary>
*** <param name="i_username">
*** The user name for login
*** </param>
*** <param name="i_password">
*** The password for login in clear text
*** </param>
*** <returns> r_result (string) </returns>
    class-methods login_user
      importing
        i_username type string
        i_password type string
      returning
        value(r_result) type string
      raising /blck/cx_api_exception.
*** <summary> method UserApi->logout_user
*** Logs out current logged in user session
*** </summary>
*** <returns>  </returns>
    class-methods logout_user
      raising /blck/cx_api_exception.
*** <summary> method UserApi->update_user
*** Updated user
*** </summary>
*** <param name="i_body">
*** Updated user object
*** </param>
*** <param name="i_username">
*** name that need to be updated
*** </param>
*** <returns>  </returns>
    class-methods update_user
      importing
        i_body type /blck/mfi_user
        i_username type string
      raising /blck/cx_api_exception.

    class-methods config
      importing
        i_basepath type string default mc_def_uri
        i_auth     type ref to /blck/api_cl_auth optional.
        
  protected section.
  
    class-data mvs_basepath type string default mc_def_uri.
    class-data mcl_auth type ref to /blck/api_cl_auth.
        
endclass.

*** <summary>
*** /blck/mfi_cl_UserApi implementation
*** </summary>
class /blck/mfi_cl_UserApi implementation.

  method config.
    mvs_basepath = i_basepath.
    mcl_auth = i_auth.
  endmethod.

*** <summary> method UserApi->create_user
*** Create user
*** </summary>
*** <param name="i_body">
*** Created user object
*** </param>
*** <returns>  </returns>
  method create_user.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'create_user' "#EC NOTEXT
*        i_parameter = 'body' "#EC NOTEXT
*        i_value = i_body ).
        
    lvs_path = '/user'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    

* handle body parameter
    data lr_data_in type /blck/mfi_user.
    lr_data_in = i_body->get_structure( ).
    lvs_postbody = to_json( lr_data_in ).
    

    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_POST
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

* no return value
  endmethod.
  
*** <summary> method UserApi->create_users_with_array_input
*** Creates list of users with given input array
*** </summary>
*** <param name="i_body">
*** List of user object
*** </param>
*** <returns>  </returns>
  method create_users_with_array_input.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'create_users_with_array_input' "#EC NOTEXT
*        i_parameter = 'body' "#EC NOTEXT
*        i_value = i_body ).
        
    lvs_path = '/user/createWithArray'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    

* handle body parameter
    lvs_postbody = to_json( i_body ).

    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_POST
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

* no return value
  endmethod.
  
*** <summary> method UserApi->create_users_with_list_input
*** Creates list of users with given input array
*** </summary>
*** <param name="i_body">
*** List of user object
*** </param>
*** <returns>  </returns>
  method create_users_with_list_input.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'create_users_with_list_input' "#EC NOTEXT
*        i_parameter = 'body' "#EC NOTEXT
*        i_value = i_body ).
        
    lvs_path = '/user/createWithList'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    

* handle body parameter
    lvs_postbody = to_json( i_body ).

    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_POST
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

* no return value
  endmethod.
  
*** <summary> method UserApi->delete_user
*** Delete user
*** </summary>
*** <param name="i_username">
*** The name that needs to be deleted
*** </param>
*** <returns>  </returns>
  method delete_user.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'delete_user' "#EC NOTEXT
*        i_parameter = 'username' "#EC NOTEXT
*        i_value = i_username ).
        
    lvs_path = '/user/{username}'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{username}'. "#EC NOTEXT
    <fs_string_map>-v = param_to_string( i_username ).
    
    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_DELETE
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

* no return value
  endmethod.
  
*** <summary> method UserApi->get_user_by_name
*** Get user by user name
*** </summary>
*** <param name="i_username">
*** The name that needs to be fetched. Use user1 for testing. 
*** </param>
*** <returns> r_result (user) </returns>
  method get_user_by_name.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'get_user_by_name' "#EC NOTEXT
*        i_parameter = 'username' "#EC NOTEXT
*        i_value = i_username ).
        
    lvs_path = '/user/{username}'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{username}'. "#EC NOTEXT
    <fs_string_map>-v = param_to_string( i_username ).
    
    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_GET
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

    create object r_result.
    
    from_json(
      exporting
        i_json = lvs_respbody
      changing
        c_model = r_result->mr_user ).
        
    r_result->assign_structure( changing c_structure = r_result->mr_user ).
        

  endmethod.
  
*** <summary> method UserApi->login_user
*** Logs user into the system
*** </summary>
*** <param name="i_username">
*** The user name for login
*** </param>
*** <param name="i_password">
*** The password for login in clear text
*** </param>
*** <returns> r_result (string) </returns>
  method login_user.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'login_user' "#EC NOTEXT
*        i_parameter = 'username' "#EC NOTEXT
*        i_value = i_username ).
        
*    assert_supplied(
*      exporting
*        i_method = 'login_user' "#EC NOTEXT
*        i_parameter = 'password' "#EC NOTEXT
*        i_value = i_password ).
        
    lvs_path = '/user/login'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    
    if i_username is not initial.
      append initial line to li_queryparams assigning <fs_string_map>.
      <fs_string_map>-k = 'username'. "#EC NOTEXT
      <fs_string_map>-v = param_to_string( i_username ).
    endif.
    
    if i_password is not initial.
      append initial line to li_queryparams assigning <fs_string_map>.
      <fs_string_map>-k = 'password'. "#EC NOTEXT
      <fs_string_map>-v = param_to_string( i_password ).
    endif.
    


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_GET
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).


  r_result = lvs_respbody.

  endmethod.
  
*** <summary> method UserApi->logout_user
*** Logs out current logged in user session
*** </summary>
*** <returns>  </returns>
  method logout_user.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
    lvs_path = '/user/logout'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_GET
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

* no return value
  endmethod.
  
*** <summary> method UserApi->update_user
*** Updated user
*** </summary>
*** <param name="i_body">
*** Updated user object
*** </param>
*** <param name="i_username">
*** name that need to be updated
*** </param>
*** <returns>  </returns>
  method update_user.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'update_user' "#EC NOTEXT
*        i_parameter = 'body' "#EC NOTEXT
*        i_value = i_body ).
        
*    assert_supplied(
*      exporting
*        i_method = 'update_user' "#EC NOTEXT
*        i_parameter = 'username' "#EC NOTEXT
*        i_value = i_username ).
        
    lvs_path = '/user/{username}'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{username}'. "#EC NOTEXT
    <fs_string_map>-v = param_to_string( i_username ).
    
    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    

* handle body parameter
    data lr_data_in type /blck/mfi_user.
    lr_data_in = i_body->get_structure( ).
    lvs_postbody = to_json( lr_data_in ).
    

    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_PUT
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

* no return value
  endmethod.
  
endclass.

