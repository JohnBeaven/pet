
*** <summary>
*** /blck/mfi_cl_StoreApi definition
*** 
*** </summary>
class /blck/mfi_cl_StoreApi definition create protected
    inheriting from /blck/api_cl.

  public section.
  
    constants mc_def_uri type string value 'https://petstore.swagger.io/v2'. "#EC NOTEXT
	
*** <summary> method StoreApi->delete_order
*** Delete purchase order by ID
*** </summary>
*** <param name="i_order_id">
*** ID of the order that needs to be deleted
*** </param>
*** <returns>  </returns>
    class-methods delete_order
      importing
        i_order_id type i
      raising /blck/cx_api_exception.
*** <summary> method StoreApi->get_inventory
*** Returns pet inventories by status
*** </summary>
*** <returns> r_result (map) </returns>
    class-methods get_inventory
      returning
        value(r_result) type /blck/api_i_mt
      raising /blck/cx_api_exception.
*** <summary> method StoreApi->get_order_by_id
*** Find purchase order by ID
*** </summary>
*** <param name="i_order_id">
*** ID of pet that needs to be fetched
*** </param>
*** <returns> r_result (order) </returns>
    class-methods get_order_by_id
      importing
        i_order_id type i
      returning
        value(r_result) type /blck/mfi_order
      raising /blck/cx_api_exception.
*** <summary> method StoreApi->place_order
*** Place an order for a pet
*** </summary>
*** <param name="i_body">
*** order placed for purchasing the pet
*** </param>
*** <returns> r_result (order) </returns>
    class-methods place_order
      importing
        i_body type /blck/mfi_order
      returning
        value(r_result) type /blck/mfi_order
      raising /blck/cx_api_exception.

    class-methods config
      importing
        i_basepath type string default mc_def_uri
        i_auth     type ref to /blck/api_cl_auth optional.
        
  protected section.
  
    class-data mvs_basepath type string default mc_def_uri.
    class-data mcl_auth type ref to /blck/api_cl_auth.
        
endclass.

*** <summary>
*** /blck/mfi_cl_StoreApi implementation
*** </summary>
class /blck/mfi_cl_StoreApi implementation.

  method config.
    mvs_basepath = i_basepath.
    mcl_auth = i_auth.
  endmethod.

*** <summary> method StoreApi->delete_order
*** Delete purchase order by ID
*** </summary>
*** <param name="i_order_id">
*** ID of the order that needs to be deleted
*** </param>
*** <returns>  </returns>
  method delete_order.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'delete_order' "#EC NOTEXT
*        i_parameter = 'order_id' "#EC NOTEXT
*        i_value = i_order_id ).
        
    lvs_path = '/store/order/{orderId}'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{orderId}'. "#EC NOTEXT
    <fs_string_map>-v = param_to_string( i_order_id ).
    
    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_DELETE
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

* no return value
  endmethod.
  
*** <summary> method StoreApi->get_inventory
*** Returns pet inventories by status
*** </summary>
*** <returns> r_result (map) </returns>
  method get_inventory.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
    lvs_path = '/store/inventory'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_GET
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

*    data lr_return type /blck/api_i_mt.

    from_json(
      exporting
        i_json = lvs_respbody
      changing
        c_model = r_result ).
        

  endmethod.
  
*** <summary> method StoreApi->get_order_by_id
*** Find purchase order by ID
*** </summary>
*** <param name="i_order_id">
*** ID of pet that needs to be fetched
*** </param>
*** <returns> r_result (order) </returns>
  method get_order_by_id.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'get_order_by_id' "#EC NOTEXT
*        i_parameter = 'order_id' "#EC NOTEXT
*        i_value = i_order_id ).
        
    lvs_path = '/store/order/{orderId}'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{orderId}'. "#EC NOTEXT
    <fs_string_map>-v = param_to_string( i_order_id ).
    
    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    


    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_GET
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

    create object r_result.
    
    from_json(
      exporting
        i_json = lvs_respbody
      changing
        c_model = r_result->mr_order ).
        
    r_result->assign_structure( changing c_structure = r_result->mr_order ).
        

  endmethod.
  
*** <summary> method StoreApi->place_order
*** Place an order for a pet
*** </summary>
*** <param name="i_body">
*** order placed for purchasing the pet
*** </param>
*** <returns> r_result (order) </returns>
  method place_order.
  
    data:
      lvs_path         type string,
      li_pathparams    type /blck/api_string_map_tt,
      li_queryparams   type /blck/api_string_map_tt,
      lvs_postbody     type string,
      li_headerparams  type /blck/api_string_map_tt,
      li_formparams    type /blck/api_string_map_tt,
      lvs_fileparams   type string,
      lcl_authsettings type ref to /blck/api_cl_auth,
      lvs_respbody     type string,
      lvxs_respbody    type xstring.
      
    field-symbols:
      <fs_string_map>   type /blck/api_string_map.
      
*    assert_supplied(
*      exporting
*        i_method = 'place_order' "#EC NOTEXT
*        i_parameter = 'body' "#EC NOTEXT
*        i_value = i_body ).
        
    lvs_path = '/store/order'. "#EC NOTEXT

    append initial line to li_pathparams assigning <fs_string_map>.
    <fs_string_map>-k = '{format}'. "#EC NOTEXT
    <fs_string_map>-v = 'json'. "#EC NOTEXT
    

* handle body parameter
    data lr_data_in type /blck/mfi_order.
    lr_data_in = i_body->get_structure( ).
    lvs_postbody = to_json( lr_data_in ).
    

    call_api(
      exporting
        i_path         = lvs_path
        i_method       = mc_meth_POST
        i_pathparams   = li_pathparams
        i_queryparams  = li_queryparams
        i_postbody     = lvs_postbody
        i_headerparams = li_headerparams
        i_formparams   = li_formparams
        i_fileparams   = lvs_fileparams
        i_authsettings = lcl_authsettings
      importing
        e_body         = lvs_respbody
        e_xbody        = lvxs_respbody ).

    create object r_result.
    
    from_json(
      exporting
        i_json = lvs_respbody
      changing
        c_model = r_result->mr_order ).
        
    r_result->assign_structure( changing c_structure = r_result->mr_order ).
        

  endmethod.
  
endclass.

